## Requirement ##

    yaourt -S i3-gaps-next-git i3lock i3status i3-blocks-gaps-git conky compton-git pavucontrol xtitle rofi feh fcitx ttf-ubuntu-font-family ttf-font-awesome

### Generate screen layout ###

We use `arandr` to generate layout file

    yaourt -S arandr
    arandr # save new layout to ~/.i3/scree.sh
